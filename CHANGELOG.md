# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security
### Changed

[Unreleased]: https://gitlab.com/tottokotkd/create-kayblis/compare/abc17285ea073dd143e79d8a64af4330091b65a2...HEAD
